class Thing:
    def __init__(self, volume, name=None):
        self._volume = volume
        self._name = name

    def volume(self):
        return self._volume

    def __repr__(self):
        return self._name

    def set_name(self,name):
        self._name = name

    def has_name(self,name):
        if self._name == name:
            return True
        else:
            return False

    