class Box:
    def __init__(self,is_open=True,capacity=None):
        self._contents = []
        self._is_open = is_open
        self._capacity = capacity

    def add(self,truc):
        self._contents.append(truc)

    def __contains__(self, machin):
        return machin in self._contents
    
    def remove(self,machin):
        self._contents.remove(machin)

    def is_open(self):
        return self._is_open

    def close(self):
        self._is_open = False

    def open(self):
        self._is_open = True

    def action_look(self):
        if not self.is_open():
            return "la boite est fermee"
        else:
            return "la boite contient: "+", ".join(self._contents)

    def set_capacity(self,capacity):
        self._capacity = capacity

    def capacity(self):
        if self._capacity == 0:
            return None
        return self._capacity

    def has_room_for(self,t):
        if self.capacity() == None:
            return True 
        return t.volume() <= self.capacity()

    def action_add(self,t):
        if self.is_open() and self.has_room_for(t):
            self.add(t)
            return True
        else:
            return False

    def find(self,name):
        if self.is_open:
            for item in self._contents:
                if item.has_name(name):
                    return item
        return None
