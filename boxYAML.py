import yaml
import io

class BoxYAML:
    def __init__(self,is_open=True,capacity=None):
        self.contents = []
        self.is_open = is_open
        self.capacity = capacity
    
    @staticmethod
    def from_yaml(data):
        i_o = data.get("is_open", None)
        ca = data.get("capacity", None)
        return BoxYAML(is_open=i_o,capacity=ca)
